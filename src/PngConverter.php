<?php

namespace Drupal\signaturefield;

use Drupal\file\FileInterface;

/**
 * Provides the PNG converter service.
 */
class PngConverter implements PngConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function fileToDataUrl(FileInterface $file_entity) {
    $contents = file_get_contents($file_entity->getFileUri());

    return $this->contentsToDataUrl($contents);
  }

  /**
   * {@inheritdoc}
   */
  public function contentsToDataUrl(string $contents) {
    return 'data:image/png;base64,' . base64_encode($contents);
  }

  /**
   * {@inheritdoc}
   */
  public function dataUrlToContents(string $data_url) {
    if (strpos($data_url, 'data:image/png;base64,') !== 0) {
      throw new \InvalidArgumentException('Only PNG image data URLs are supported.');
    }

    $contents = mb_substr($data_url, 22);
    $contents = str_replace(' ', '+', $contents);

    return base64_decode($contents);
  }

}
