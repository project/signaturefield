<?php

namespace Drupal\signaturefield\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Provides the signature file field type.
 *
 * @FieldType(
 *   id = "signature_file",
 *   label = @Translation("Signature file"),
 *   description = @Translation("An entity field referencing a PNG image file entity that contains a signature."),
 *   category = @Translation("General"),
 *   default_widget = "signature_file_pad",
 *   default_formatter = "signature",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class SignatureFileItem extends FileItem implements SignatureItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    $default = parent::defaultStorageSettings();

    if ($default['uri_scheme'] === 'private') {
      return $default;
    }

    // The "private" uri scheme is preferred if available.
    $schemes = \Drupal::service('stream_wrapper_manager')->getNames(StreamWrapperInterface::WRITE_VISIBLE);
    if (isset($schemes['private'])) {
      $default['uri_scheme'] = 'private';
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      'file_extensions' => 'png',
      'file_directory' => 'signaturefield',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $element['display_field']['#access'] = FALSE;

    if (isset($element['display_default'])) {
      $element['display_default']['#access'] = FALSE;
    }

    $uri_scheme = &$element['uri_scheme'];
    $uri_scheme['#title'] = $this->t('File destination');
    $uri_scheme['#description'] = $this->t("Select where the signature image files should be stored. <strong>It's highly encouraged to use a private destination that's not publicly accessible.</strong>");

    if (!isset($uri_scheme['#options']['public']) || $uri_scheme['#default_value'] === 'public') {
      return $element;
    }

    if (count($uri_scheme['#options']) === 2) {
      $uri_scheme['#access'] = FALSE;
    }
    else {
      unset($uri_scheme['#options']['public']);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::fieldSettingsForm($form, $form_state);
    $element['file_extensions']['#access'] = FALSE;
    $element['max_filesize']['#access'] = FALSE;
    $element['description_field']['#access'] = FALSE;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageSrc(): ?string {
    $file = $this->entity;
    if ($file instanceof FileInterface) {
      return $file->createFileUrl();
    }

    return NULL;
  }

}
