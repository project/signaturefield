<?php

namespace Drupal\signaturefield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides the signature field type.
 *
 * @FieldType(
 *   id = "signature",
 *   label = @Translation("Signature"),
 *   description = @Translation("An entity field containing a signature."),
 *   category = @Translation("General"),
 *   default_widget = "signature_pad",
 *   default_formatter = "signature",
 * )
 */
class SignatureItem extends FieldItemBase implements SignatureItemInterface {

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();

    return empty($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getImageSrc(): ?string {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];

    $properties['value'] = DataDefinition::create('uri')
      ->setLabel(t('Data URL'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'big',
        ],
      ],
    ];
  }

}
