<?php

namespace Drupal\signaturefield\Plugin\Field\FieldType;

/**
 * Provides an interface for signature field items.
 */
interface SignatureItemInterface {

  /**
   * Get the field item's image src attribute.
   *
   * @return string|null
   *   The image src attribute or NULL if empty.
   */
  public function getImageSrc(): ?string;

}
