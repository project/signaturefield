<?php

namespace Drupal\signaturefield\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\signaturefield\Plugin\Field\FieldType\SignatureFileItem;
use Drupal\signaturefield\pngConverterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the signature pad widget for the signature file field.
 *
 * @FieldWidget(
 *   id = "signature_file_pad",
 *   label = @Translation("Signature file pad"),
 *   field_types = {
 *     "signature_file",
 *   },
 *   multiple_values = FALSE,
 * )
 */
class SignatureFilePadWidget extends SignaturePadWidget {

  /**
   * The PNG converter service.
   *
   * @var \Drupal\signaturefield\PngConverterInterface
   */
  protected PngConverterInterface $pngConverter;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * Class constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\signaturefield\PngConverterInterface $png_converter
   *   The PNG converter service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, PngConverterInterface $png_converter, FileSystemInterface $file_system, FileRepositoryInterface $file_repository) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->pngConverter = $png_converter;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('signaturefield.png_converter'),
      $container->get('file_system'),
      $container->get('file.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $file = $items[$delta]->entity;

    if (!$file instanceof FileInterface) {
      return $element;
    }

    $data_url = $this->pngConverter->fileToDataUrl($file);

    $element['value']['#default_value'] = $data_url;

    $element['original_file_id'] = [
      '#type' => 'value',
      '#value' => $file->id(),
    ];

    $element['original_data_url'] = [
      '#type' => 'value',
      '#value' => $data_url,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $parents = array_merge($form['#parents'], [$this->fieldDefinition->getName()]);

    foreach ($values as $delta => $value) {
      // Build the dummy form element to check for validation errors.
      $element = ['#parents' => $parents];
      $element['#parents'][] = $delta;
      $element['#parents'][] = 'value';

      if ($value['value'] === '' || $form_state->getError($element) !== NULL) {
        // None or an invalid value submitted.
        $fid = NULL;
      }
      elseif (isset($value['original_data_url']) && $value['original_data_url'] === $value['value']) {
        // Use the existing file if the data URL didn't change.
        $fid = $value['original_file_id'];
      }
      else {
        // Hash the signature data URL to use as key.
        $key = md5($value['value']);

        if ($form_state->has(['signature_file_pad_fids', $key])) {
          // Reuse the previously created file.
          $fid = $form_state->get(['signature_file_pad_fids', $key]);
        }
        else {
          // Create a new file.
          $fid = $this->createFileFromDateUrl($value['value'])->id();
          $form_state->set(['signature_file_pad_fids', $key], $fid);
        }
      }

      $values[$delta] = [
        'target_id' => $fid,
      ];
    }

    return $values;
  }

  /**
   * Create a PNG image file from a data URL.
   *
   * @param string $data_url
   *   The data URL.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFileFromDateUrl(string $data_url) {
    // Get the destination.
    $data_definition = FieldItemDataDefinition::create($this->fieldDefinition);
    $destination = (new SignatureFileItem($data_definition))->getUploadLocation();

    // Ensure the destination is writable.
    if (!$this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      throw new FileWriteException('The destination directory "' . $destination . '" is not writable');
    }

    // Generate a random filename.
    $destination .= '/' . (new Random())->name(10) . '.png';

    // Create the file.
    $file = $this->fileRepository->writeData(
      $this->pngConverter->dataUrlToContents($data_url),
      $destination
    );

    // Make it temporary.
    $file->setTemporary();
    $file->save();

    return $file;
  }

}
