<?php

namespace Drupal\signaturefield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the signature pad widget for the signature field.
 *
 * @FieldWidget(
 *   id = "signature_pad",
 *   label = @Translation("Signature pad"),
 *   field_types = {
 *     "signature",
 *   },
 *   multiple_values = FALSE,
 * )
 */
class SignaturePadWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'width' => 400,
      'height' => 200,
      'pen_color' => 'black',
      'background_color' => 'rgba(0, 0, 0, 0)',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
      '#min' => 200,
      '#step' => 10,
      '#required' => TRUE,
      '#field_suffix' => 'px',
    ];

    $element['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
      '#min' => 100,
      '#step' => 10,
      '#required' => TRUE,
      '#field_suffix' => 'px',
    ];

    $element['pen_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pen color'),
      '#description' => $this->t('A color name, hex code or in rgb(a) format.'),
      '#default_value' => $this->getSetting('pen_color'),
      '#required' => TRUE,
    ];

    $element['background_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background color'),
      '#description' => $this->t('A color name, hex code or in rgb(a) format.'),
      '#default_value' => $this->getSetting('background_color'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Dimensions: @widthx@heightpx', [
      '@width' => $this->getSetting('width'),
      '@height' => $this->getSetting('height'),
    ]);

    $summary[] = $this->t('Pen color: @pen, background color: @background', [
      '@pen' => $this->getSetting('pen_color'),
      '@background' => $this->getSetting('background_color'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'signature',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#width' => $this->getSetting('width'),
      '#height' => $this->getSetting('height'),
      '#pen_color' => $this->getSetting('pen_color'),
      '#background_color' => $this->getSetting('background_color'),
    ];

    return $element;
  }

}
