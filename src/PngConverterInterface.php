<?php

namespace Drupal\signaturefield;

use Drupal\file\FileInterface;

/**
 * Provides an interface for the PNG converter service.
 */
interface PngConverterInterface {

  /**
   * Convert a PNG image file into its data URL.
   *
   * @param \Drupal\file\FileInterface $file_entity
   *   The file entity referencing a PNG image.
   *
   * @return string
   *   The data URL.
   */
  public function fileToDataUrl(FileInterface $file_entity);

  /**
   * Convert the contents of a PNG image file into its data URL.
   *
   * @param string $contents
   *   The contents of a PNG image file.
   *
   * @return string
   *   The data URL.
   */
  public function contentsToDataUrl(string $contents);

  /**
   * Convert a PNG image data URL into its file contents.
   *
   * @param string $data_url
   *   The data URL.
   *
   * @return string
   *   The file contents.
   */
  public function dataUrlToContents(string $data_url);

}
