Signature (Field)
=================

Provides a signature form element and field.


## Requirements

* [Signature Pad](https://github.com/szimek/signature_pad)


## Installation

Start by adding asset-packagist and the required installer configuration to your `composer.json` file:

```json
{
    "extra": {
        "installer-paths": {
            "web/libraries/{$name}": [
                "type:drupal-library",
                "type:bower-asset",
                "type:npm-asset"
            ]
        },
        "installer-types": [
            "bower-asset",
            "npm-asset"
        ]
    },
    "repositories": {
        "asset-packagist": {
            "type": "composer",
            "url": "https://asset-packagist.org"
        }
    }
}
```

Next download the latest release and its dependencies using `composer require drupal/signaturefield`.

Now you can simply enable the module as described in the [the module installation guide](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Usage

### Form element

You can add a signature element in a custom form using following code:

```php
$form['signature'] = [
  '#type' => 'signature',
  '#title' => t('Sign here'),
  '#width' => '400', // Width in pixels, defaults to 400.
  '#height' => '200', // Height in pixels, defaults to 200.
  '#pen_color' => 'black',  // Color used to draw the lines. Can be any color format accepted by context.fillStyle. Defaults to "black".
  '#background_color' => 'rgba(0, 0, 0, 0)', // Color used to clear the background. Can be any color format accepted by context.fillStyle. Defaults to "rgba(0,0,0,0)" (transparent black).
  '#default_value' => NULL, // Signature image as data URL.
];
```

The submitted value will be the signature as a PNG in [data URL](https://en.wikipedia.org/wiki/Data_URI_scheme) format, e.g. `data:image/png;base64,iVBORw0KGgoAAA...`.


### Field

You can create a `Signature` or `Signature file` field:

* The `Signature` field stores the signature as a data URL in a blob field, which might lead to a bigger database.
* The `Signature file` field stores the signature as a file on the file system, **which is not secure if you use a public storage**.
