Changelog
=========

All notable changes to the Signature (Field) module.


## 1.1.0 (2023-11-13):

### Added

* Drupal 10 support
* Support for saving signatures as files (#3292083 by mdolnik)

### Fixed

* Signature being cleared on browser resize (#3292082 by mdolnik)

### Changed

* Minimum dimensions (#3299812 by mdolnik)

### Removed

* Drupal 8 support


## 1.0.1 (2022-02-09):

### Added

* Dependency on oomphinc/composer-installers-extender

### Changed

* Blob size of the value column (#3259705)


## 1.0.0 (2021-09-27):

First release.
