/**
 * @file
 * The signature library.
 */

(function (window, document, Drupal) {

  Drupal.behaviors.signaturefield = {
    attach: function (context, settings) {
      once('signaturefield', 'canvas', context).forEach(function (canvas) {
        var canvasId = canvas.getAttribute('id');
        if (!canvasId || !canvasId in settings.signaturefield) {
          return;
        }

        var config = settings.signaturefield[canvasId];
        var valueElement = document.getElementById(config.valueId);

        // Create the signature pad.
        var signaturePad = null;
        signaturePad = new SignaturePad(canvas, {
          penColor: config.penColor,
          backgroundColor: config.backgroundColor,
          onEnd: function () {
            valueElement.value = signaturePad.toDataURL();
          }
        });

        // Bind and trigger the resize callback.
        var resizeCanvas = function () {
          // Resize the canvas.
          var ratio = Math.max(window.devicePixelRatio || 1, 1);
          canvas.width = canvas.offsetWidth * ratio;
          canvas.height = canvas.offsetHeight * ratio;
          canvas.getContext('2d').scale(ratio, ratio);

          // Populate the value.
          var value = valueElement.value;
          if (value.indexOf('data:image/') === 0) {
            signaturePad.fromDataURL(value);
          }
        };

        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        // Bind the clear signature link.
        document.querySelector('a[href="#' + canvasId + '"]').onclick = function (event) {
          signaturePad.clear();
          valueElement.value = '';
          event.target.blur();

          return false;
        };
      });
    }
  };

})(window, document, Drupal);
